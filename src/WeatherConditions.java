import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * WEATHER CONDITIONS
 * This class defines the Weather Conditions
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-02
 */
// I used the following tag in order to ignore data from JSON object that is not mapped in this class.
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherConditions {

    // I used the following tag in order to rename the nested main block and map its values.
    @JsonProperty("main")
    private Map<String, Float> measurements;
    private String name;

    /**
     * Default Weather Conditions constructor
     */
    public WeatherConditions() {
        this.name = null;
        this.measurements = null;
    }

    /**
     * This function gets the name of the city.
     *
     * @return the city's name
     */
    public String getName() {
        return name;
    }

    /**
     * This function sets the name of the city.
     *
     * @param name is the name of the city.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This function gets the map inside the main nested block.
     *
     * @return a map of the main nested block.
     */
    public Map<String, Float> getMeasurements() {
        return measurements;
    }

    /**
     * This function sets the values inside the nested main block.
     *
     * @param measurements the measures of the weather.
     */
    public void setMeasurements(Map<String, Float> measurements) {
        this.measurements = measurements;
    }

    /**
     * This function return the measurements conditions from the nested block main
     *
     * @return measurements data
     */
    public String getCurrentCondition(String attribute) {
        return this.measurements.get(attribute).toString();
    }

    /**
     * This function displays the weather conditions of the city.
     */
    public String toString() {
        return "Current weather condition in: " + this.name + "\n" +
                "The temperature is: " + getCurrentCondition("temp") + " F" + "\n" +
                "The feels like is: " + getCurrentCondition("feels_like") + " F" + "\n" +
                "The pressure is: " + getCurrentCondition("pressure") + " hpa" + "\n" +
                "The humidity is: " + getCurrentCondition("humidity") + " %";
    }
}
